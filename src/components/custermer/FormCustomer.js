import React, { Component } from "react";
import { Table, Input, Button, Space, Row, Col, Form, Checkbox } from "antd";
class FormCustomer extends Component {
  render() {
    return (
      <div>
        <Form
          ref={this.customerForm}
          name="basic"
          initialValues={{ remember: true }}
          onFinish={this.onFinish}
          onFinishFailed={this.onFinishFailed}
        >
          <Form.Item
            label="Mã vé"
            name="mave"
            rules={[{ required: true, message: "Please input your username!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Số ghế"
            name="soghe"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Mã chuyến bay"
            name="chuyenbay"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Giá"
            name="gia"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Thêm
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export default FormCustomer;
