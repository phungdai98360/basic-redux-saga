import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchCustomerAction, addTicketAction } from "./../../actions/index";
import { Table, Input, Button, Space, Row, Col, Form, Checkbox } from "antd";
import Highlighter from "react-highlight-words";
import { SearchOutlined } from "@ant-design/icons";
import * as actionType from "./../../actions/actionType";
class Custermer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: "",
      searchedColumn: "",
      listTicket: [],
      status: false,
      update: false,
      mave: "",
      soghe: "",
      macb: "",
      gia: 0,
      selectedData: null,
    };
  }
  customerForm = React.createRef();

  async componentDidMount() {
    this.props.getlistCustomer();
  }
  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };
  componentWillReceiveProps(nextProps) {
    const arrTicket = [];
    if (nextProps.listCustermer.length > 0) {
      for (let i = 0; i < nextProps.listCustermer.length; i++) {
        arrTicket.push({
          mave: nextProps.listCustermer[i].mave,
          soghe: nextProps.listCustermer[i].soghe,
          gia: nextProps.listCustermer[i].gia
            ? nextProps.listCustermer[i].gia
            : "500000",
          dadat: !nextProps.listCustermer[i].dada ? "Trống" : "Đã đặt",
          chuyenbay:
            nextProps.listCustermer &&
            nextProps.listCustermer.length > 0 &&
            nextProps.listCustermer[i].chuyenbay
              ? nextProps.listCustermer[i].chuyenbay.macb
              : "no",
        });
      }
      this.setState({
        listTicket: arrTicket,
      });
    }
  }
  onFinish = (values) => {
    console.log("Success:", values);
    this.props.addTicket(values);
  };

  onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  onAddTicket = () => {
    this.setState({
      status: !this.state.status,
    });
  };
  onUpdate = (record) => {
    console.log(record);
    this.setState(
      {
        selectedData: record,
        //  update:true,
        status: true,
        //  macb:record.macb,
        //  soghe:record.soghe,
        //  gia:record.gia,
        //  macb:record.chuyenbay
      },
      () => {
        console.log("form: ", this.customerForm.current);
        this.customerForm.current.setFieldsValue({
          ...record,
        });
      }
    );
  };
  render() {
    //console.log(this.state.listTicket[0], this.state.listTicket && this.state.listTicket.length > 0 ? this.state.listTicket[0].chuyenbay : "no value");

    const columns = [
      {
        title: "Mã vé",
        dataIndex: "mave",
        key: "mave",

        ...this.getColumnSearchProps("mave"),
      },
      {
        title: "Số ghế",
        dataIndex: "soghe",
        key: "soghe",

        ...this.getColumnSearchProps("soghe"),
      },
      {
        title: "Giá",
        dataIndex: "gia",
        key: "gia",

        ...this.getColumnSearchProps("email"),
      },
      {
        title: "Đã đặt",
        dataIndex: "dadat",
        key: "dadat",

        ...this.getColumnSearchProps("dadat"),
      },
      {
        title: "Chuyến bay",
        dataIndex: "chuyenbay",
        key: "chuyenbay",

        ...this.getColumnSearchProps("chuyenbay.macb"),
      },
      {
        title: "Hành động",
        key: "action",
        render: (text, record) => (
          <React.Fragment>
            <Button onClick={() => this.onUpdate(record)}>Sửa</Button>
            <Button>Xóa</Button>
          </React.Fragment>
        ),
      },
    ];
    console.log("rerender: ", this.state.selectedData);
    return (
      <div>
        <Row>
          <Col>
            <Button onClick={this.onAddTicket}>Thêm</Button>
          </Col>
        </Row>
        <Row>
          {this.state.status === true ? (
            <Col xs={12}>
              <Form
                ref={this.customerForm}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
              >
                <Form.Item
                  label="Mã vé"
                  name="mave"
                  rules={[
                    { required: true, message: "Please input your username!" },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  label="Số ghế"
                  name="soghe"
                  rules={[
                    { required: true, message: "Please input your password!" },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label="Mã chuyến bay"
                  name="chuyenbay"
                  rules={[
                    { required: true, message: "Please input your password!" },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label="Giá"
                  name="gia"
                  rules={[
                    { required: true, message: "Please input your password!" },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    Thêm
                  </Button>
                </Form.Item>
              </Form>
            </Col>
          ) : (
            ""
          )}
          <Col xs={this.state.status === true ? 12 : 24}>
            <Table columns={columns} dataSource={this.state.listTicket} />
          </Col>
        </Row>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    listCustermer: state.customer,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getlistCustomer: () => {
      dispatch(fetchCustomerAction());
    },
    addTicket: (data) => {
      dispatch({ type: actionType.ADD_TICKET, values: data });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Custermer);
