import callApi from './../../util/callApi';
const url="http://datvemaybay.somee.com/api/ve/get-all";
function* getCustomerFromApi(){
    const response=yield callApi("GET",url,null);
    const customer=response.data;
    return customer;
}
function* addTicketFromApi(data){
    let params={
        mave:data.mave,
        soghe:data.soghe,
        macb:data.macb,
        gia:data.gia
    }
   let result=yield callApi("POST","http://datvemaybay.somee.com/api/ve/insert",params);
   return result;
}
export const Api={
    getCustomerFromApi,
    addTicketFromApi
}