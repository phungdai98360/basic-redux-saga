import * as actionType from './../actionType';
import {put,takeLatest,takeEvery,call} from 'redux-saga/effects';
import {Api} from './apis';
import axios from 'axios';
function* fetCustermer(){
    try {
        const data=yield Api.getCustomerFromApi();
        yield put({
            type:actionType.FETCH_CUSTOMER_SUCCESS,
            data:data
        })
    } catch (error) {
        yield put({
            type:actionType.FETCH_CUSTOMER_FAIL,
            error
        })
    }
}
function* addTicket(action){
    try {
        const uri="http://datvemaybay.somee.com/api/ve/insert?";
        const result=yield call(Api.addTicketFromApi,action.values);
        console.log(result);
        yield put({ type: actionType.FETCH_CUSTOMER});
    } catch (error) {
       console.log(error)
    }
}
export function* watchFetCustomer(){
    yield takeLatest(actionType.FETCH_CUSTOMER,fetCustermer);
}
export function* watchAddTicket(){
    yield takeEvery(actionType.ADD_TICKET,addTicket);
}