import {call,all} from 'redux-saga/effects'
import {watchFetCustomer,watchAddTicket} from './index';
export default function* rootSaga(){
    yield all([
        watchFetCustomer(),
        watchAddTicket()
    ])
}