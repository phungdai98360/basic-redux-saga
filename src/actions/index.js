import * as actionType from './actionType';
export const fetchCustomerAction=()=>{
    return {
        type:actionType.FETCH_CUSTOMER
    }
}
export const fetchCustomerSuccess=(data)=>{
    return {
        type:actionType.FETCH_CUSTOMER_SUCCESS,
        data
    }
}
export const fetchCustomerFail=(err)=>{
    return {
        type:actionType.FETCH_CUSTOMER_FAIL,
        err
    }
}
