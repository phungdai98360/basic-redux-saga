import * as actionType from './../actions/actionType';
const customer=(state=[{id:1,name:'dai'}],action)=>{
    switch (action.type) {
        case actionType.FETCH_CUSTOMER_SUCCESS:
            state=action.data
            return [...state]
        case actionType.FETCH_CUSTOMER_FAIL:
            return []  
        case actionType.ADD_TICKET:
             let data=action.values;
             state.push(data);
             return [...state]    
        default:
            return [...state];
    }
}
export default customer;