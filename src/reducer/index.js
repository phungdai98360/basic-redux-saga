import {combineReducers} from 'redux';
import customer from './customer';
const reducer=combineReducers({
    customer
});
export default reducer;